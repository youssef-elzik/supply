(function ($) {
    "use strict";

    // Print Orders

    $("#print").click(function(){
        window.print();
    })

    // Display Products Hint

    $("#productNameinput").focus(function(){
        $("#productHint").addClass('show');
    })

    $("#productNameinput").blur(function(){
        $("#productHint").removeClass('show');
    })

    // Display Sellers Hint

    // $("#sellerInput").focus(function(){
    //     $("#sellerBox").addClass('show-box');
    // })

    $('.sellerInput').each(function(){
        $(this).focus(function(){
            $(".sellerBox").each(function(){
                $(this).addClass('show-box');
            })
            
            // console.log("object")
        })
    })

    $("#sellersList").click(function(){
        $("#sellerBox").removeClass('show-box');
            $("#otherSeller").removeClass('hide')
            $("#newSeller").removeClass("show-input");
        
    })

    $(document).keyup(function(e) {
        if (e.key === "Escape") { 
            $("#sellerBox").removeClass('show-box');
        }
    });
    $("#otherSeller").click(function(){
        $(this).addClass("hide");
        $("#newSeller").addClass("show-input");
    })
    

    // Add Element
    $("#addProduct").click(function() {
        $("#defaultElement").prepend(`
        <div class="user-info">
            <div class="row align-items-center">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="ProductName">Product Name</label>
                        <input type="text" class="form-control" id="ProductName" name="ProductName" placeholder="Type" autocomplete="off">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="Count">Count</label>
                        <input type="text" class="form-control" id="Count" name="Count" placeholder="Type" autocomplete="off">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group seller-dropdown">
                        <label for="sellerInput">Seller</label>
                        <input type="text" class="form-control sellerInput" id="sellerInput" name="sellerInput" placeholder="Choose" autocomplete="off">
                        <div class="sellers-box sellerBox" id="sellerBox">
                            <ul class="sellers-list" id="sellersList">
                                <li>
                                    <h6>Ibnsina pharma</h6>
                                    <div class="price">
                                        <span class="presentage">20%</span>
                                        <span class="number">180 EGP</span>
                                    </div>
                                </li>
                                <li>
                                    <h6>Ibnsina pharma</h6>
                                    <div class="price">
                                        <span class="presentage">20%</span>
                                        <span class="number">180 EGP</span>
                                    </div>
                                </li>
                                <li>
                                    <h6>Ibnsina pharma</h6>
                                    <div class="price">
                                        <span class="presentage">20%</span>
                                        <span class="number">180 EGP</span>
                                    </div>
                                </li>
                            </ul>
                            <div class="other-seller" id="otherSeller">
                                Other Seller
                            </div>
                            <div class="new-seller" id="newSeller">
                                <input type="text" placeholder="Type another seller here" name="newSeller">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="add-btn" id="addProduct">
                        <img src="../assets/images/remove.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        `)
    })

    // Mobile Menu
    $('#mobileMenu').click(function(e){
        e.preventDefault();
        $('.sidebar').toggleClass('show-menu')
    })

    // Show Uploaded Image

    $('#uploadImageShow').change(function(e){
            
        var file = e.target.files[0]

        if (file) {
            $("#srcImage").attr('src', URL.createObjectURL(file))
        }
    })

    // Line Charts

    var ctx = document.getElementById('myChart');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Sep', 'Oct', 'Nov', 'Dec'],
                datasets: [{
                    label: 'Delivered',
                    data: [20, 15, 25, 10, 15, 10, 20, 15, 25, 5, 20, 15],
                    backgroundColor: [
                        'rgba(31, 70, 152, 1)',
                    ],
                    maxBarThickness: 5,
                    borderRadius:2
                },
                {
                    label: 'Declined',
                    data: [20, 15, 25, 10, 15, 10, 20, 15, 25, 5, 20, 15],
                    backgroundColor: [
                        'rgba(228, 85, 85, 1)'
                    ],
                    maxBarThickness: 5,
                    borderRadius:2
                }
            ]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });

    // Circle Chart

    var ctx = document.getElementById('myChart2');
        var myChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                
                datasets: [{
                    data: [25, 25, 25, 25],
                    backgroundColor: [
                        'rgb(31, 70, 152)',
                        'rgb(255, 99, 132)',
                        'rgb(54, 162, 235)',
                        'rgb(255, 205, 86)'
                    ],
                }
            ]
            }
        });

})(jQuery);
